from DMT.core import Technology

from pylatex import Section
from DMT.external.pylatex import Tex
from DMT.sEKV import McSekv


class TechSky130(Technology):
    default_mcard = McSekv()
    name = "Sky130"

    def __init__(self):
        super().__init__(self, "Sky130")


    def print_tex(self, dut_ref, mcard):
        """Prints a technology description, mainly used for autodocumentation reasons.

        Parameters
        ----------
        dut_ref : :class:`~DMT.core.DutView`
            Can be used to obtain tech quanties... (or to generate a TRADICA input file :) )
        mcard : :class:`~DMT.core.McParameterCollection`
            A Modelcard that contains all parameters that are required for scaling, as well as the parameters that shall be scaled.
        """
        doc = Tex()
        with doc.create(Section("Technology :" + self.name)):
            doc.append(
                "Measurement on the Sky130 raw data test tile and extraction of sEKV model"
            )
        #TODO
        return doc

    def get_bib_entries(self):
        """bibliograpy entries of a technology"""
        return """@ARTICLE{9896232,
                author={Han, Hung-Chi and D’Amico, Antonio and Enz, Christian},
                journal={IEEE Open Journal of Circuits and Systems}, 
                title={SEKV-E: Parameter Extractor of Simplified EKV I-V Model for Low-Power Analog Circuits}, 
                year={2022},
                volume={3},
                number={},
                pages={162-167},
                doi={10.1109/OJCAS.2022.3179046}}
                """

    def scale_modelcard(
        self, mcard, lE0, bE0, nfinger, config, lE_drawn_ref=None, bE_drawn_ref=None
    ):
        #TODO
        return mcard

""" Extract sEKV
"""
from pathlib import Path
from DMT.config import COMMANDS

COMMANDS["OPENVAF"] = "openvaf"
from DMT.core import DutLib, specifiers, DutType
from DMT.ngspice import DutNgspice
from DMT.xyce import DutXyce
from DMT.extraction import Xtraction, XVerify
from DMT.gui import XtractionGUI
from DMT.sEKV import (
    McSekv,
    McEkv,
    XIds,
    XGmsNormN,
    XGmsNormIspec,
    XGmsNormFull,
)
from tech_sky130 import TechSky130
import numpy as np

path_curr = Path(__file__).parent

# Switch for automated extraction
automated_extraction = True
# Switch for ekv model
sekv = True

## modelcard
if sekv:
    mc = McSekv()
else:
    mc = McEkv()

## Verilog-A model
model = mc.get_verilogae_model()


## lib stores all data with Pandas
lib = DutLib.load(path_curr / "skywater130-pdk-dutlib" / "skywater130_lib")
lib.dut_ref = next(
    dut
    for dut in lib
    if dut.dut_type == DutType.n_mos and np.isclose(dut.length, 0.5e-6)
)

## technology
tech = TechSky130()

## specifiers
col_temp = specifiers.TEMPERATURE
col_vds = specifiers.VOLTAGE + ["D", "S"]
col_vgs = specifiers.VOLTAGE + ["G", "S"]
col_vb = specifiers.VOLTAGE + "B"
col_id = specifiers.CURRENT + "D"

## extraction object
extraction = Xtraction(
    "EKV",
    mc,
    path_curr / "Extraction",
    lib,
    autosave_pushed_modelcards="code_compressed",
)
if sekv:
    step_n = XGmsNormN(  # extraction of n in weak inversion
        "1:N",
        mc,
        lib,
        {
            col_temp: 300,
            col_vds: 5,
            col_vgs: (0.3, None),
            col_vb: 0,
        },
        model,
        key="IDVG",
        to_optimize=["n"],
        specifier_outer_sweep=col_vds,
    )
    step_n.x_bounds = [(3e-09, 5e-07)]
    extraction.add_xstep(step_n)
    step_ispec = XGmsNormIspec(  # extraction of ispec in weak inversion
        "2: Ispec",
        mc,
        lib,
        {
            col_temp: 300,
            col_vds: 5,
            col_vgs: (0.3, None),
            col_vb: 0,
        },
        model,
        key="IDVG",
        to_optimize=["ispec"],
        specifier_outer_sweep=col_vds,
    )
    step_ispec.x_bounds = [(1e-6, 1)]
    extraction.add_xstep(step_ispec)
step_gm_full = XGmsNormFull(  # numerical optimization of ispec, n, lambdac
    "3: gm optimization",
    mc,
    lib,
    {
        col_temp: 300,
        col_vds: 5,
        col_id: (1e-6, None),
        col_vb: 0,
    },
    model,
    key="IDVG",
    to_optimize=["lambdac", "vt0"] if sekv else ["phi", "gamma"],
    specifier_outer_sweep=col_vds,
)
step_gm_full.x_bounds = [(3e-09, 0.017)]
extraction.add_xstep(step_gm_full)
step_full = XIds(  # where are other Vbc
    "4 :final optimization",
    mc,
    lib,
    {
        col_temp: 300,
        col_vds: 5,
        col_vb: 0,
    },
    model,
    key="IDVG",
    to_optimize=["ispec", "n", "vt0", "lambdac"]
    if sekv
    else ["lambda", "vto", "avto", "ucex", "akp", "theta"],
    specifier_outer_sweep=col_vds,
)
step_full.x_bounds = [(0.5, 4)]
extraction.add_xstep(step_full)

# implementation of automated extraction
if automated_extraction:
    for i_step, step in extraction.iter_steps():
        step.extract()

## verify steps
verify_id_ngspice = XVerify(
    "5:circuit simulation ngspice",
    mc,
    lib,
    op_definition={
        col_temp: 300,
        col_vds: 5,
        col_vb: 0,
    },
    DutCircuitClass=DutNgspice,
    quantity_fit=col_id,
    key="IDVG",
    to_optimize=["n", "lambdac", "ispec", "vt0"]
    if sekv
    else ["lambda", "vto", "avto", "ucex", "akp", "theta"],
    technology=tech,
    fit_along=col_vgs,
    outer_sweep_voltage=col_vds,
    inner_sweep_voltage=col_vgs,
    verify_area_densities=False,
    relevant_duts=[lib.dut_ref],
)

verify_id_ngspice.switch_plots(
    {
        "plot_ic": False,
        "plot_ib": False,
        "plot_ft": False,
    }
)
verify_id_ngspice.switch_prints(
    {
        "main_plot": True,
        "plot_ib": False,
        "plot_ic": False,
        "plot_ft": False,
    }
)
extraction.add_xstep(verify_id_ngspice)
verify_id_xyce = XVerify(
    "6:circuit simulation xyce",
    mc,
    lib,
    op_definition={
        col_temp: 300,
        # col_vds: 5,
        col_vb: 0,
    },
    DutCircuitClass=DutXyce,
    quantity_fit=col_id,
    key="IDVG",
    # use_exact_key=True,
    to_optimize=["n", "lambdac", "ispec", "vt0"],
    technology=tech,
    fit_along=col_vgs,
    outer_sweep_voltage=col_vds,
    inner_sweep_voltage=col_vgs,
    verify_area_densities=False,
    relevant_duts=[lib.dut_ref],
)

verify_id_xyce.switch_plots(
    {
        "plot_ic": False,
        "plot_ib": False,
        "plot_ft": False,
    }
)
verify_id_xyce.switch_prints(
    {
        "main_plot": True,
        "plot_ib": False,
        "plot_ic": False,
        "plot_ft": False,
    }
)
if sekv:
    extraction.add_xstep(verify_id_xyce)

gui = XtractionGUI(extraction)
gui.start()


extraction.mcard.dump_json(path_curr / "mcard_result.json")

# Demo_DMT_sEKV

Demo project to show DMT extraction capabilities

## Getting started

1. clone this repository to your local machine

2. cd into the repository and create a virtual environment

> virtualenv venv -p python3.10

3. active virtual environment and install DMT-core

> source venv/bin/activate.sh
> pip install numpy, cycler, matplotlib
> pip install dmt-core

4. clone DMT_sekv repository and cd into it, then

> pip install -e .


3. either create the Skywater SKY130 raw data DutLib from the submodule or download it from the [release](https://gitlab.com/dmt-development/skywater130-pdk-dutlib/-/releases/07538b1f) and unpack to skywater130-pdk-dutlib/skywater130_lib
4. add DMT_sEKV to DMT as a submodule by merging from master
5. pip install DMT_sEKV with dependencies
6. final target: have extract_sekv.py working
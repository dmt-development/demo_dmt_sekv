import verilogae
import numpy as np
from DMT.sEKV import default_va_files, McSekv, McEkv
from DMT.core import Plot

vgs = np.linspace(0.5,1,61)

# sEKV model interface for python:
model_sekv = verilogae.load(default_va_files.sEKV)
# modelcard from verilogae without implementing anything manually:
modelcard_sekv = McSekv()
# evaluate model functions without implementing them manually in milliseconds:
id_sekv = model_sekv.functions["i"].eval(
    temperature=300, 
    voltages={
        "br_gs": vgs, 
        "br_ds": 5,
    }, 
    **modelcard_sekv.to_kwargs()
)
paras_sekv = model_sekv.functions["i"].parameters

# same works for full EKV model (and any other Verilog-A model):
model_ekv = verilogae.load(default_va_files.EKV)
modelcard_ekv = McEkv()
id_ekv = model_ekv.functions["Id"].eval(
    temperature=300, 
    voltages={
        "br_gs":vgs, 
        "br_ds":5, 
        "br_sb":0, 
        "br_gb":vgs, 
        "br_db":5,
    }, 
    **modelcard_ekv.to_kwargs()
)
paras_ekv = model_ekv.functions["i"].parameters

#plot the results
plt_ekv = Plot("ID(VG)", x_label=r"$V_{\mathrm{GS}} \left( \si{\volt} \right)$", y_label=r"$I_{\mathrm{D}} \left( \si{\ampere} \right)$", y_log=True)
plt_ekv.add_data_set(vgs, id_sekv, label="sEKV")
plt_ekv.add_data_set(vgs, id_ekv, label="EKV")
plt_ekv.plot_py(show=True)

# => enables highly modular integration of Verilog-A models into Python frameworks



dummy = 1